/**
 * Created by Bence Kormos <bence.kormos@kormiqus.tech> on 29/11/2016.
 */

export default class IndexedDictionary {

  static simplify(val) {
    if (val === null) return 'null';
    const type = typeof val;
    if (type === 'undefined') return 'undefined';
    if (Array.isArray(val) || type === 'object') return JSON.stringify(val);
    return val;
  }

  /**
   * @constructor
   * @param {Array} items
   * @param {object} options
   */
  constructor(items = [], options = {}) {
    this._primaryKey = (options.primaryKey || 'id');
    this._fields = new Map();
    this._items = [];
    this._subscribers = [];

    this.addKey(this._primaryKey);
    this.addKeys(options.keys || []);

    items.forEach(this.add, this);
  }

  /**
   * @param args
   * @return {IndexedDictionary}
   * @private
   */
  _notify(...args) {
    this._subscribers.forEach(f => f(...args));
    return this;
  }

  /**
   * @param {string} key
   * @param {object} item
   * @param {number} position
   * @private
   */
  _indexItemByKey(key, item, position) {
    const oldItem = this.at(position);
    const field = this._fields.get(key);
    const value = IndexedDictionary.simplify(item[key]);

    // remove old - if exists
    if (oldItem) {
      const oldValue = this.at(position)[key];
      const oldIndex = (field[oldValue] || []).indexOf(position);
      if (oldIndex > -1) {
        field[oldValue].splice(oldIndex, 1);
        if (field[oldValue].length === 0) {
          delete field[oldValue];
        }
      }
    }

    (field[value] = field[value] || []);
    const index = field[value].indexOf(position);
    if (index === -1) {
      field[value].push(position);
    } else {
      field[value].splice(index, 1, position);
    }
  }

  addKey(key) {
    if (this._fields.has(key)) return this;
    this._fields.set(key, {});
    this._items.forEach(this._indexItemByKey.bind(this, key));
    return this;
  }

  addKeys(...keys) {
    keys.forEach(this.addKey, this);
    return this;
  }

  add(item) {
    const { _fields, _primaryKey, _items } = this;
    const primaryValue = item[_primaryKey];
    if (primaryValue == null) throw "Primary value cannot be null or undefined!";
    const primaryField = _fields.get(_primaryKey);
    const positions = primaryField[primaryValue];
    const oldItem = JSON.parse(JSON.stringify(positions ? this.at(positions[0]) : null));
    const position = (positions ? positions[0] : _items.length);
    const keys = _fields.keys();
    let key;
    while(key = keys.next().value) {
      this._indexItemByKey(key, item, position);
    }
    this._items.splice(position, 1, item);
    this._notify(oldItem, this.at(position), position);
    return this;
  }

  /**
   * Convenience method
   * @see {IndexedDictionary#add}
   */
  update(item) {
      return this.add(item);
  }

  at(index) {
    return this._items[index];
  }

  get(id) {
    return this.where(this._primaryKey, id)[0];
  }

  where(key, value) {
    const positions = this._fields.get(key)[value];
    return positions.map(this.at, this);
  }

  subscribe(f) {
    this._subscribers.push(f);
    return this;
  }

  unsubscribe(f) {
    const index = this._subscribers.indexOf(f);
    if (index > -1) {
      this._subscribers.splice(index, 1);
    }
    return this;
  }

  remove(item) {
    // TODO: remove item
    // TODO: notify removal (oldItem, null, position)
  }

}
